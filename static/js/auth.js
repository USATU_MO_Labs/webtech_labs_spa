function updateAuth() {
    var loginButton = $("#loginButton");
    var logoutButton = $("#logoutButton");
    
    if (APP_STATE.logged_in) {
        loginButton.hide();
        logoutButton.show();
    } else {
        loginButton.show();
        logoutButton.hide();
    }
}


function login() {
    APP_STATE.logged_in = true;
    updateAuth();
}


function logout() {
    APP_STATE.logged_in = false;
    updateAuth();
}


// TODO доделать, когда появится сервер
$("#loginSubmitButton").click(function (e) {
    e.preventDefault();
    login();
    setTimeout(function() {$("#loginModal").modal("hide")}, 750);
})


$("#logoutButton").click(function (e) {
    e.preventDefault();
    logout();
});