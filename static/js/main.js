// конфигурация приложения
const APP_CONFIG = {
    routes: [
        { path: "/", destination: "/freshMemes" },
        { path: "/freshMemes", $view: $("#content-freshMemes") },
        { path: "/accordionExample", $view: $("#content-accordion") },
        { path: "/carouselExample", $view: $("#content-carousel") },
        { path: "/autocompleteExample", $view: $("#content-autocomplete") },
        { path: "/datepickerExample", $view: $("#content-datepicker") },
        { path: "/about", $view: $("#content-about") }
    ],

    global_animation_duration: 400
};


// состояние приложения
var APP_STATE = {
    $current_view: null,

    logged_in: false
};


// обходит таблицу путей, пытаясь сопоставить данный путь и вьюху
// обрабатывает редиректы
function resolveRoute(route) {
    var route_item = { destination: route };

    while (route_item != undefined && route_item.destination) {
        route_item = APP_CONFIG.routes.find(function (item) {
            return item.path == route_item.destination
        });
    }

    if (!route_item || !route_item.$view) {
        console.error("Unable to match route '" + route + "' with any of declared views");
        return;
    }

    return route_item.$view;
}


// сокрытие текущей и загрузка новой вьюхи
function loadView(route) {
    var $view = resolveRoute(route);
    if (!$view) return;

    // не загружаем заново, если уже загружен
    if (APP_STATE.$current_view == $view) return;

    if (APP_STATE.$current_view) {
        APP_STATE.$current_view.hide(APP_CONFIG.global_animation_duration);
    }

    $view.show(APP_CONFIG.global_animation_duration);
    APP_STATE.$current_view = $view;
}


// установка обработчика для переключения вьюх
$(function () {
    $("[router-route]").click(function (e) {
        e.preventDefault();

        var path = $(this).attr("router-route");
        loadView(path);
    });
});


// инициализация
$(function () {
    // элементы jquery ui
    $("#accordion").accordion();
    // $("#carouselExampleIndicators .carousel-item").carouselHeights();

    // инициализация - прячем все вьюхи
    var view_routes = APP_CONFIG.routes.filter(function (route) {
        return route.$view != undefined
    });

    for (var route of view_routes) {
        route.$view.hide();
    }

    // обновляем элементы и данные в соответствием со статусом аутентификации
    updateAuth();

    // загрузка домашней вьюхи
    loadView("/");
});
